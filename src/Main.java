public class Main{
    public static void main(String[] args){

        Pessoa contato1 = new Pessoa("Guilherme Rodrigues", 989905177, 17, 1.85f);
        Pessoa contato2 = new Pessoa("Guilherme Santos", 989905176, 16, 1.84f);
        Pessoa contato3 = new Pessoa("Santos Rodrigues", 989905175, 15, 1.83f);
        Pessoa contato4 = new Pessoa("Rodrigues Santos", 989905174, 14, 1.82f);
        Pessoa contato5 = new Pessoa("Rodrigues Guilherme", 989905173, 13, 1.81f);


        Agenda agenda = new Agenda();

        agenda.armazenarPessoa(contato1);
        agenda.armazenarPessoa(contato2);
        agenda.armazenarPessoa(contato3);
        agenda.armazenarPessoa(contato5);
        agenda.armazenarPessoa(contato4);

        agenda.imprimirAgenda();

        agenda.buscaPessoa(contato2);
        agenda.removerPessoa(contato2);

        agenda.imprimirAgenda();

        agenda.imprimePessoa(2);

    }
}