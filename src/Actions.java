public interface Actions {

    public abstract void armazenarPessoa(Pessoa contato);

    public abstract void removerPessoa(Pessoa contato);

    public abstract void buscaPessoa(Pessoa contato);

    public abstract void imprimirAgenda();

    public abstract void imprimePessoa(int index);

}