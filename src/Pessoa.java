public class Pessoa{

    // ATTRIBUTES
    private String nome;
    private int numero;
    private int idade;
    private float altura;

    // METHODS
    // Construtor
    public Pessoa(String name, int numero, int idade, float altura) {
        setNome(name);
        setNumero(numero);
        setIdade(idade);
        setAltura(altura);
    }

    // Getters and Setters ~ (Métodos acessores e modificadores)
    public String getNome() { return this.nome; }
    public void setNome(String nome) { this.nome = nome; }

    public int getNumero() { return this.numero; }
    public void setNumero(int numero) { this.numero = numero; }

    public int getIdade() { return this.idade; }
    public void setIdade(int idade) { this.idade = idade; }

    public float getAltura() { return this.altura; }
    public void setAltura(float altura) { this.altura = altura; }

}