import java.util.ArrayList;

public class Agenda implements Actions {

    // ATTRIBUTES
    ArrayList<Pessoa> agenda = new ArrayList<>();

    // METHODS
    public void armazenarPessoa(Pessoa contato) {
//        int aux = 1;
//        for (Pessoa contt : agenda) {
//            if (contato.getNumero() == contt.getNumero()) {
//                System.out.println("{ERROR} O contato já está adicionado.");
//                break;
//            }
//            aux++;
//        }
//        if (aux == agenda.size())
        agenda.add(contato);
    }

    public void removerPessoa(Pessoa contato) {
        if (agenda.contains(contato))
            agenda.remove(agenda.indexOf(contato));
        else
            System.out.println("O contato não está adicionado à agenda");
    }

    public void buscaPessoa(Pessoa contato) {
        int aux = 1;
        for (Pessoa contt : agenda)
            if (contato.getNome() == contt.getNome()) {
                System.out.println(contato.getNome() + " foi encontrado na posição " + (agenda.indexOf(contt)+1));
                aux++;
            }
        if (aux == agenda.size())
            System.out.println(contato.getNome() + " não foi encontrado na agenda");
    }

    public void imprimirAgenda() {
        int i = 1;
        for (Pessoa contt : agenda) {
            System.out.println(i + " : [  NOME: " + contt.getNome() + ", NÚMERO: " + contt.getNumero() + ", IDADE: " + contt.getIdade() + ", ALTURA: " + contt.getAltura() + "  ]");
            i++;
        }
    }

    public void imprimePessoa(int index) {
        Pessoa contato = agenda.get(index);
        System.out.println(agenda.indexOf(contato) + " : [  Nome: " + contato.getNome() + ", Número: " + contato.getNumero() + ", Idade: " + contato.getIdade() + ", Altura: " + contato.getAltura());
    }

}
